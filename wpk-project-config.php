<?php

/*
Plugin Name:  WPK Project Configurator
Project:      Test Project
Plugin URI:   https://wpkonsulterna.se/
Description:  Configuration plugin for syncing themes and plugins between wordpress installs.
Version:      0.1
Author:       WP Konsulterna
Author URI:   https://wpkonsulterna.se/
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  wpk-project-config
Domain Path:  /languages
Bitbucket Plugin URI: https://rossjames96@bitbucket.org/wpkonsulterna/sample-wpk-project-configurator.git
*/

/**
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1 for plugin WPK Project Configurator
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */

require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'wpk_project_config_register_required_plugins' );
add_action( 'tgmpa_register', 'wpk_project_config_register_general_plugins' );

require_once dirname( __FILE__ ) . '/tgm-general-plugins.php';
require_once dirname( __FILE__ ) . '/tgm-project-plugins.php';