#!/bin/bash

########################################
# About this script
########################################

# This script is to be used to prepare a new project install on wpengine after it has been created.


########################################
# How to use this script
########################################
# 1) Clone down the repository to your environment
# 2) Set the correct permission on the script file to be able to execute it, 'chmod 755 wpengine-setup.sh'
# 3) Run script through the command ./wpengine-setup.sh


########################################
# Set variables
########################################

# Initial hostname
initialhostname=$HOSTNAME

# Accept user input for the wpengine install name
read -p 'WP Engine install name: ' wpeinstall

# Accept user input for the subdomain name
read -p 'Subdomain name (e.g. if ericsson.wpkonsulterna.se then the correct value is ericsson): ' wpsubdomain

# Define siteurl
siteurl="https://$wpsubdomain.wpkonsulterna.se"

# Generate new password
newpassword=$(date +%s | sha256sum | base64 | head -c 18)

# Folder to create for project configuration plugin
pluginfolder="sites/$wpeinstall/wp-content/plugins/wpk-config-$wpsubdomain"

########################################
# Login to WP Engine install through SSH
########################################

# Run as 'here docs' (<< EOF) to be able to run bash script commands after ssh connection
ssh -i ~/.ssh/wpengine_rsa -o IdentitiesOnly=yes $wpeinstall@$wpeinstall.ssh.wpengine.net /bin/bash << EOF

printf "\n
########################################
# Post, Page and Comments Configuration
########################################
\n"

# Delete default Posts, Pages and Comments
wp post delete 1 2
wp comment delete 1

# Create pages and save their id's to be used later
homeid=\$(wp post create --post_type=page --post_title='Home' --post_status=publish --porcelain)
blogid=\$(wp post create --post_type=page --post_title='Blog' --post_status=publish --porcelain)


printf "\n
########################################
# Theme configuration
########################################
\n"

# Install and activate BB Builder Theme
wp theme install https://s3-eu-west-1.amazonaws.com/wpk-public-assets/WP+Themes/bb-theme.zip --activate

# Create bb-theme-child folder ready for DeployHQ
sudo mkdir -p wp-content/themes/bb-theme-child/

# Delete unused themes (but keep one for potential troubleshooting)
wp theme delete twentyfifteen twentysixteen

# Create and set main menu
menuid=\$(wp menu create "Main Navigation" --porcelain)
wp menu location assign \$menuid header
wp menu item add-post \$menuid \$homeid --title="Home"
wp menu item add-post \$menuid \$blogid --title="Blog"


printf "\n
########################################
# Option Configuration
########################################
\n"

# Set Site Title & Tagline
wp option update blogname '$wpsubdomain'
wp option update blogdescription ''

# Set WordPress Address and Site Address
wp option update siteurl '$siteurl'
wp option update home '$siteurl'

# Set time, date and timezone
wp option update date_format "Y-m-d"
wp option update time_format "H:i"
wp option update gmt_offset 2

# Set Homepage & Posts page
wp option update show_on_front "page"
wp option update page_on_front \$homeid
wp option update page_for_posts \$blogid

# Change Default article settings to no allow people to post comments on new articles
wp option update default_comment_status "closed"

# Set correct permalinks
wp rewrite structure "/%postname%/"


printf "\n
########################################
# User Configuration
########################################
\n"

# Update admin user with generated password
wp user update service@wpkonsulterna.se --user_pass=$newpassword


printf "\n
########################################
# Plugin Configuration
########################################
\n"

# Delete akismet
wp plugin delete akismet

# Add TGMPA WP CLI package
# wp package install itspriddle/wp-cli-tgmpa-plugin
# wp tgmpa-plugin install --all-required --activate

# Install and activate ManageWP
wp plugin install worker --activate

# Create folder for the project configuration plugin to later be deployed to by DeployHQ
mkdir -m 775 $pluginfolder

EOF

printf "\nSSH Status Code: "
printf $?
printf "\n"

if [ $? -eq 00 ]
then 

########################################
# Write out list of actions that should be done now after the script
########################################
printf "\nNow when the setup is done a couple of manual steps needs to be done:\n"
printf "1) Add the new WordPress user password to LastPass: $newpassword\n"
printf "2) Login to https://orion.managewp.com/dashboard/add-website and add this site with the url: $siteurl\n"
printf "3) You might want to clear the WP Engine page cache to have all these changes visable for everyone: https://my.wpengine.com/installs/$wpeinstall/utilities\n"
printf "\n"

else

printf "\nSomething with the SSH connection went wrong\n"

fi