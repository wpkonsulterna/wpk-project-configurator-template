#!/bin/bash

########################################
# Set variables
########################################
# accept user input for the databse name
read -p 'DeployHQ API key: ' deployhqapikey


curl -0 -v -X POST https://wpkonsulterna.deployhq.com/projects/ \
-H "Content-type: application/json" \
-H "Accept: application/json" \
--user service@wpkonsulterna.se:$deployhqapikey \
-d @- << EOF

{
  "project":{
    "name":"My Project"
  }
}
EOF