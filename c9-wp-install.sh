#!/bin/bash

########################################
# About this script
########################################

# This script is to be used to setup a new WordPress install on your local dev environment.


########################################
# How to use this script
########################################
# 1) Clone down the repository to your environment
# 2) Set the correct permission on the script file to be able to execute it, 'chmod 755 c9-wp-install.sh'
# 3) Run script through the command ./c9-wp-install.sh


########################################
# Set variables
########################################

# Accept user input for the wpengine install name
read -p 'Install folder / name: ' installname

# Generate new password
newpassword=$(date +%s | sha256sum | base64 | head -c 18)

# Define siteurl
siteurl="https://$C9_HOSTNAME/$installname"

mkdir $installname

cd $installname

wp core download

wp config create --dbname=$installname --dbuser=$C9_USER --dbpass= --dbhost=$IP

wp db create

wp core install --url=https://$C9_HOSTNAME/$installname --title=$1 --admin_user=wpkonsulterna --admin_password=$newpassword --admin_email=service@wpkonsulterna.se --skip-email

printf "\n
########################################
# Post, Page and Comments Configuration
########################################
\n"


printf "\n
########################################
# Theme configuration
########################################
\n"

# Install and activate BB Builder Theme
wp theme install https://s3-eu-west-1.amazonaws.com/wpk-public-assets/WP+Themes/bb-theme.zip --activate

# Delete unused themes (but keep one for potential troubleshooting)
wp theme delete twentyfifteen twentysixteen


printf "\n
########################################
# Option Configuration
########################################
\n"

# Set WordPress Address and Site Address
wp option update siteurl '$siteurl'
wp option update home '$siteurl'

# Set correct permalinks
wp rewrite structure "/%postname%/"


printf "\n
########################################
# Plugin Configuration
########################################
\n"

# Delete akismet
wp plugin delete akismet

# Add TGMPA WP CLI package
# wp package install itspriddle/wp-cli-tgmpa-plugin
# wp tgmpa-plugin install --all-required --activate

# Install and activate ManageWP
wp plugin install worker --activate

# Install All-in-one Migrate
wp plugin install all-in-one-wp-migration --activate

# Create folder for the project configuration plugin to later be deployed to by DeployHQ
# mkdir -m 775 $pluginfolder


########################################
# Write out list of actions that should be done now after the script
########################################
printf "\nNow when the setup is done a couple of manual steps needs to be done:\n"
printf "1) Add the new WordPress user password to LastPass: $newpassword\n"
printf "2) Login to https://orion.managewp.com/dashboard/add-website and add this site with the url: $siteurl\n"
printf "3) You might want to clear the WP Engine page cache to have all these changes visable for everyone: https://my.wpengine.com/installs/$wpeinstall/utilities\n"
printf "\n"
printf "Please note that it might take a minute or 2 before the changes show correctly in the browser\n"
printf "\n"